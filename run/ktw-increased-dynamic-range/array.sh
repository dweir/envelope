#!/bin/bash
#SBATCH --job-name=ktw
#SBATCH -e envelope_err_%j
#SBATCH -o envelope_out_%j
#SBATCH --account=Project_2003695
#SBATCH --partition=small
#SBATCH --time=08:00:00
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=4000
#SBATCH --array=1-500

outfile=out.`printf '%03d' ${SLURM_ARRAY_TASK_ID}`

time srun python3 ../../envelope.py config-denser | tee $outfile

