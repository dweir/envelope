#!/bin/bash
#SBATCH --job-name=ktw
#SBATCH -e envelope_err_%j
#SBATCH -o envelope_out_%j
#SBATCH --account=Project_2003695
#SBATCH --partition=small
#SBATCH --time=01:00:00
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=1000
#SBATCH --array=1-96

outfile=out.`printf '%03d' ${SLURM_ARRAY_TASK_ID}`

srun python3 ../../envelope.py config-denser | tee $outfile
