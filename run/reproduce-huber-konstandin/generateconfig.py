beta = 1.0
vb = 1.0
L = 7.0*vb/beta
tmax = L/vb

filehuber = open('config-huber','w')
filehuber.write('L %g\n' % L)
filehuber.write('tmax %g\n' % tmax)
filehuber.write('vw %g\n' % vb)
filehuber.write('bubblelist bubblelist-huber\n')
filehuber.write('omega 0.00147, 0.00316, 0.00681, 0.0147, 0.0316, 0.0681, 0.147\n')
filehuber.write('# not sure exactly how many samples were used, check from elsewhere\n')
filehuber.write('samples 512\n')
filehuber.write('kmin 4\n')
filehuber.write('kmax 15\n')
filehuber.write('images true\n')
filehuber.write('cutoff false\n')
filehuber.close()

filebubbles = open('bubblelist-huber','w')

import random,math,numpy.random,sys

nbubbles = 109

def get_t(beta,tmax):
    imax = 4000
    for i in range(imax):
        t = (float(i)/imax)*tmax
        prob = 400*math.exp(beta*t)/(400*math.exp(beta*tmax))
#        sys.stderr.write('t %g prob %g\n' % (t,prob))
        if prob > random.uniform(0,1):
#            sys.stderr.write('yes\n')
            return t
    sys.stderr.write('oops\n')
    sys.exit(1)


tset = []
for i in range(nbubbles):
    tset.append(get_t(beta,tmax))

min_t = max(tset)
if min_t > tmax:
    sys.stderr.write('Incompatible maximum of nucleation set!\n')
    sys.exit(1)

rev_t = sorted([min_t - t for t in tset])

prevbubbles = []

for t0, i in zip(rev_t,range(nbubbles)):
    okay = False

    while not okay:

        okay = True

        x0 = random.uniform(-L/2.0,L/2.0)
        y0 = random.uniform(-L/2.0,L/2.0)
        z0 = random.uniform(-L/2.0,L/2.0)

        for x1, y1, z1, t1 in prevbubbles:
            dx = abs(x1-x0)
            if dx > L/2:
                dx = L - dx
            dy = abs(y1-y0)
            if dy > L/2:
                dy = L - dy
            dz = abs(z1-z0)
            if dz > L/2:
                dz = L - dz

            dist = math.sqrt(dx*dx + dy*dy + dz*dz)
            if dist < vb*abs(t0-t1):
                okay = False

    print i, t0

    filebubbles.write('(%g, %g, %g) %g\n' % (x0, y0, z0, t0))
#    filebubbles.write('%g %g %g %g\n' % (x0, y0, z0, t0))
    prevbubbles.append((x0,y0,z0,t0))
