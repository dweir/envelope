# recentre.py
#
# Quick and dirty python script for 'recentering' bubble locations
# supplied as a comma-separated list (straight from a simulation, and
# in the range [0,L]) and outputs a list of bubbles that have been
# centred about the origin (thus in the range [-L/2,L/2]) and which
# have the nucleation time set to 0.0, i.e. simultaneous nucleation.

import sys, string, math

L = 2400.0

for line in sys.stdin.readlines():
    bits = string.split(string.strip(line)[1:-1],',')
    print '(%d, %d, %d) %g' % (2.0*(int(bits[0])-L/2.0),
                               2.0*(int(bits[1])-L/2.0),
                               2.0*(int(bits[2])-L/2.0), 0.0)
