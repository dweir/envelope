#!/bin/bash
#SBATCH -J envelope
#SBATCH -e env-mpi_err_%j
#SBATCH -o env-mpi_out_%j
#SBATCH -t 24:00:00
#SBATCH --mem-per-cpu=2000M
#SBATCH -n 125

## Set up job environment
source /cluster/bin/jobsetup


mpirun python ../envelope_mpi.py config-convert
