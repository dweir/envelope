#!/usr/bin/env python

import sys, math, string

# nb best used with
# find ./envelope-out.* | grep "/out" | xargs python ./average.py

# efficiency factor, i.e. kappaphi*rho_vac/Tc
kappaphi=0.000429602

def mean(vals):
    return sum(vals)/len(vals)

def std(vals):
    truemean = mean(vals)

    total = 0.0
    
    for i in range(len(vals)):
        total += (vals[i]-truemean)*(vals[i]-truemean)

    return math.sqrt(total/float(len(vals)-1))


# should be exactly the same
#def jackmean(vals):
#    total = 0.0
#
#    for i in range(len(vals)):
#        thismean = mean(vals[:i] + vals[i+1:])
#        total += thismean
#
#    return total/float(len(vals))


def jackstd(vals):
    truemean = mean(vals)
    total = 0.0
    
    for i in range(len(vals)):
        jackmean = mean(vals[:i] + vals[i+1:])
        total += (jackmean-truemean)*(jackmean-truemean)

    return math.sqrt(float(len(vals)-1)*total/float(len(vals)))


files = sys.argv[1:]

for i in range(40):
    k = 0.0
    vals = []

    for thefile in files:
        try:
            line = open(thefile,'r').readlines()[i+1]

            bits = string.split(string.strip(line))
            k = (float(bits[0]))
            thepow = kappaphi*kappaphi*float(bits[1])
            vals.append(thepow)
        except IndexError:
            # Doesn't have it, never mind
            continue




    if len(vals) == 0:
        break

    err = std(vals)*math.sqrt(1.0/float(len(vals)))
    ejack = jackstd(vals) # *math.sqrt(1.0/float(len(vals)))

    print k, mean(vals), err, ejack, len(vals)


