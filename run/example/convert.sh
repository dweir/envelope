# Converts the log file of a hydro simulation, with nucleated bubble
# locations, to a envelope approximation config file
#
# Needs editing for different parameter choices.

cat $1 | grep "Nucleating at" | awk '{print $3}' | python ../recentre.py  > bubblelist-convert
echo "L 800" > config-convert
echo "tmax 1000" >> config-convert
echo "vw 0.44" >> config-convert
echo "bubblelist bubblelist-convert" >> config-convert

