#!/usr/bin/env python

# Average power spectrum from multiple input files
# note that this version does not scale by kappa_phi!

import sys, math, string

# nb best used with
# find ./envelope-out.* | grep "/out" | xargs python ./average.py

files = sys.argv[1:]

for i in range(10):
    k = 0.0
    totpow = 0.0
    totpowsq = 0.0
    count = 0
    for thefile in files:
        try:
            line = open(thefile,'r').readlines()[i]
        except IndexError:
            # Doesn't have it, never mind
            continue

        bits = string.split(string.strip(line))
        k = float(bits[0])
        thepow = float(bits[1])
        totpow += thepow
        totpowsq += thepow*thepow
        count += 1

    if not count:
        break

    err = math.sqrt(totpowsq/float(count-1) - totpow*totpow/float(count*(count-1)))

    print k, totpow/float(count), err, count


