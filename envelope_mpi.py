#!/usr/bin/env python

# MPI version of envelope approximation calculation - uses envelope.py
# for nearly everything which does not need to be parallelised

from envelope import *

import math, sys, os, string, random, time
from mpi4py import MPI


comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()


def stderr0(stuff):
    if rank == 0:
        sys.stderr.write(stuff)

def stdout0(stuff):
    if rank == 0:
        sys.stdout.write(stuff)


# Share out the required integration tasks between the nodes available
def fairshare(nodes, tasks):
    lst = list(range(tasks))
    n = nodes

    # from
    # <http://stackoverflow.com/questions/2659900/
    # python-slicing-a-list-into-n-nearly-equal-length-partitions>
    res = [ lst[i::n] for i in range(n) ]

    # paranoia
    if not (sum([len(i) for i in res]) == tasks):
        sys.stderr.write('Internal consistency error while dividing tasks!\n')
        sys.exit(99)

    return res







# Use all the above to compute the power for a given set of bubbles
# (inside the box), image bubbles (counted for collisions but do not
# themselves contribute to the power), omega (frequency) and tmax
# (maximum time to which we integrate)
def get_power_mpi(bubble_list, image_list, omega, tmax, L, omega_max, samples, kmin, kmax, cutoff):




        allocs = fairshare(size, len(bubble_list))
        myallocs = allocs[rank]

        sys.stderr.write('Rank %d gets bubbles: %s\n' % (rank, repr(myallocs)))

        rlist = []

        for i in myallocs:


#            sys.stderr.write('rank %d making up interactions for bubble %d\n'
#                             % (rank, i))

            rlist.append(get_cplusminus(bubble_list[i],
                                        bubble_list[:i]
                                        + bubble_list[i+1:]
                                        + image_list,
                                        omega,
                                        tmax, 0.0,
                                        omega_max,
                                        samples, kmin, kmax, cutoff))


        cplus_total = 0.0
        cminus_total = 0.0

        cplus_total_here = 0.0
        cminus_total_here = 0.0

        for cplus, cminus in rlist:
            cplus_total_here += cplus
            cminus_total_here += cminus

        sys.stderr.write('Rank %d, cplus total %s\n'
                         % (rank, repr(cplus_total_here)))


        sys.stderr.write('Rank %d, cminus total %s\n'
                         % (rank, repr(cminus_total_here)))


        cplus_total = comm.allreduce(cplus_total_here)
        cminus_total = comm.allreduce(cminus_total_here)

        stderr0('cplus total is (%g,%g)\n'
                         % (cplus_total.real,cplus_total.imag))

        integrand = abs(cplus_total)*abs(cplus_total) \
            + abs(cminus_total)*abs(cminus_total)

        # kappa, G, (?rhovac) scaled out
        norm = 4.0*omega*omega*math.pow(bubble_list[0].vw,6.0)

        # non-Huber-Konstandin factors: an extra omega
        # and a 4*pi so that the spherical monte carlo integration
        # is a direct average.
        extranorm = omega*4.0*math.pi/float(L*L*L)

        return extranorm*norm*integrand





def main():

    stderr0('2016 Envelope code: MPI version\n')

    bubble_list = []
    image_list = []

    # Read the file bublist for a list of the bubbles to simulate
    # (todo: make this also contain parameters)
    try:
        inputfile = sys.argv[1]
        config = parse_config(inputfile)
    except IndexError:
        stderr0('Usage: %s <input file>\n' % sys.argv[0])
        sys.exit(1)
    except IOError:
        stderr0('Could not open file %s\n' % inputfile)
        sys.exit(1)

        

    try:
        L = config['L']
        tmax = config['tmax']
        vw = config['vw']
        samples = config['samples']
        kmin = config['kmin']
        kmax = config['kmax']
        images = config['images']
        cutoff = config['cutoff']
    except KeyError as e:
        stderr0('essential parameters not specified: %s missing\n'
                % e.message)
        sys.exit(1)

    stderr0('L=%d, tmax=%g, vw=%g\n' % (L, tmax, vw))

    try:
        stderr0('Reading bubbles from %s\n' % config['bubblelist'])
        lines = open(config['bubblelist'],'r').readlines()
    except KeyError:
        stderr0('bubble list not specified\n')
        sys.exit(1)

    bubble_index = 1

    for line in lines:

        line = line.strip()
        if len(line) == 0 or line[0] == '#':
            continue

        # A line of the input file should read:
        # "(x0,y0,z0) t0"

        # The coordinates come first
        coords_end = line.find(')')
        coords = line[:coords_end+1]

        # Then the nucleation time
        t0 = float(line[coords_end+1:])

        # Process the coordinates
        bits = coords[1:-1].split(',')

        if len(bits) == 3:
            x0 = float(bits[0])
            y0 = float(bits[1])
            z0 = float(bits[2])

            # sys.stderr.write('adding bubble at (%g,%g,%g), t0=%g\n'
            #                 % (x0,y0,z0,t0))

            # Instantiate a bubble and add it to the list
            bubble_list.append(Bubble(vw=vw, x0=x0, y0=y0, z0=z0, t0=t0, index=bubble_index))

            # And add the corresponding image bubbles
            if images:
                image_list += get_images(bubble_list[-1], L)

            bubble_index += 1

    # We assume that this run is a sample for a Monte Carlo integration
    # over the z-direction chosen for the cylindrical coordinate system.

    if rank == 0:
        state = random.getstate()
    else:
        state = None

    state = comm.bcast(state, root=0)

    random.setstate(state)

    angle1 = random.uniform(-math.pi,math.pi)
    z = random.uniform(-1,1)
    angle2 = math.acos(z)
    angle3 = random.uniform(-math.pi,math.pi)

    stderr0('angles are: %g, %g, %g\n' % (angle1, angle2, angle3))

    for i in range(len(bubble_list)):
        bubble_list[i].rotateZYZ(angle1, angle2, angle3)

    for i in range(len(image_list)):
        image_list[i].rotateZYZ(angle1, angle2, angle3)


    omega_max = max(config['omega'])

    # Finally, make the power spectrum
    for omega in config['omega']:

        stderr0('starting omega=%g\n' % omega)

        start = time.time()
        integrand = get_power_mpi(bubble_list, image_list, omega,
                                  tmax, L, omega_max, samples, kmin, kmax, cutoff)
        end = time.time()

        stdout0('%g %g\n' % (omega, integrand))

        stderr0('omega=%g took %d seconds walltime\n' % (omega, end-start))

        # We are impatient and it might take a while.
        sys.stdout.flush()


if __name__ == '__main__':
    main()
