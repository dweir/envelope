#!/usr/bin/env python

# Serial envelope approximation code
#
# Takes a configuration file as its only argument. The format of that file
# is along the lines of:
#
#########################################
# # Simulation volume
# L 3
#
# # tmax: highest time to integrate out to
# tmax 3.0
#
# # vw: the wall velocity
# vw 1.0
#
# # bubblelist: where the list of bubbles can be found
# bubblelist bubblelist
#
# # omega: list of frequencies to try
# omega 0.1, 1, 10, 100
#########################################
#
# The bubble list is along the lines of:
# (x0,y0,z0) t0
# - where the numbers can all be floating point, for example:
# (0.1,0.2,0.3) 10.4
#
#
# NB: The file envelope-mpi.py reimplements the parts of this file that
# are different for the parallel case, but otherwise depends on the
# logic here.

import math, sys
import string
import scipy.integrate
import cmath
import random
import time


# Floating point effective tolerance
tol = 1e-15


# This class models a bubble
class Bubble:

    def __init__(self, vw, x0, y0, z0, t0=0, index=0):
        self.vw = vw
        self.x0 = x0
        self.y0 = y0
        self.z0 = z0
        self.t0 = t0

        self.index = index

        self.radius = vw*t0

    # Rotate about the z-axis
    def rotate_z(self, phi):

        newx = self.x0*math.cos(phi) - self.y0*math.sin(phi)
        newy = self.x0*math.sin(phi) + self.y0*math.cos(phi)
        newz = self.z0

        self.x0 = newx
        self.y0 = newy
        self.z0 = newz


    # Rotate about the y-axis
    def rotate_y(self, phi):

        newx = self.x0*math.cos(phi) - self.z0*math.sin(phi)
        newy = self.y0
        newz = self.x0*math.sin(phi) + self.z0*math.cos(phi)
        

        self.x0 = newx
        self.y0 = newy
        self.z0 = newz

    # Rotate about the x-axis
    def rotate_x(self, phi):

        newx = self.x0
        newy = self.y0*math.cos(phi) - self.z0*math.sin(phi)
        newz = self.y0*math.sin(phi) + self.z0*math.cos(phi)
        

        self.x0 = newx
        self.y0 = newy
        self.z0 = newz


    # Z-Y-Z rotation
    # if a uniform rotation is desired then must sample carefully
    # phi1, phi3 in [-pi,pi]
    # z in [-1,1]
    # phi2 = acos(z)
    def rotateZYZ(self, phi1, phi2, phi3):
        self.rotate_z(phi1)
        self.rotate_y(phi2)
        self.rotate_z(phi3)




    # Get the smallest and largest values of z (normalised cylindrical
    # coordinate in [-1,1]) that _might_ intersect with anotherbubble
    #
    # Arguments:
    # - anotherbubble: the other bubble of interest
    # - t: time
    # - rb1: our radius (to safe recomputing it)
    # - rb2: the other bubble's radius
    #
    # Returns: None if there is definitely no interval of z that
    # might intersect, ohterwise returns a list of two floats.
    #
    # See the documentation for a geometric illustration of the
    # trigonometry involved.
    def get_zlimits(self, anotherbubble, t, rb1, rb2):

        # One of the bubbles has not been nucleated yet
        if t < anotherbubble.t0 or t < self.t0:
            return None

        # Separation in x, y, z
        dx = anotherbubble.x0 - self.x0
        dy = anotherbubble.y0 - self.y0
        dz = anotherbubble.z0 - self.z0

        # Distance between bubbles
        R = math.sqrt(dx*dx + dy*dy + dz*dz)

        # bubbles do not overlap
        if rb1 + rb2 < R:
            return None

        # angle A: angle between bubble z-positions and horizontal
        A = math.asin(abs(dz)/R)

        # cannot find angle C, shouldn't get here I think
        if abs((rb1*rb1 + R*R - rb2*rb2)/(2*rb1*R)) > 1.0:
            sys.stderr.write('bubble %d: no solutions to cosine rule, '
                             'bubble (%g,%g,%g) has been swallowed(?)\n'
                             % (self.index,
                                anotherbubble.x0,
                                anotherbubble.y0,
                                anotherbubble.z0))
            return None

        # angle C: cosine rule to get angle A plus angle to minimum z
        C = math.acos((rb1*rb1 + R*R - rb2*rb2)/(2*rb1*R))

        # angle B: angle between the horizontal and the line to the
        # lower bubble intersection, less than zero, it's below the
        # horizontal
        B = A - C

        # angle D: angle of the line to the upper bubble intersection
        # with the vertical
        D = math.pi/2.0 - A - C


        zmin = math.sin(B)
        zmax = math.cos(D)

        # Bubble stretches beyond the top
        if D < 0:
            zmax = 1.0

        # Bubble stretches beyond the bottom
        if B < -math.pi/2:
            zmin = -1.0

        # We took abs(dz) above, so swap signs so zmin < zmax
        if dz < 0:
            zmax = -zmax
            zmin = -zmin

        if zmax < zmin:
            return zmax, zmin

        return zmin, zmax


    # Get minimum and maximum cylindrical coordinate angles at which
    # the bubble intersects. These should always be in the range [-pi,pi]
    # 
    # Arguments:
    # - anotherbubble: the other bubble of interest
    # - t: time
    # - zmid: the value of z (for this bubble) at which we wish to
    #         know the range of angles
    # - rb1: our radius (to safe recomputing it)
    # - rb2: the other bubble's radius
    #
    # Returns -pi,pi if the whole range is covered. Only one range
    # need be returned because bubbles are spherical and so cannot
    # intersect more than once (compare with a cigar shaped bubble!).
    def get_philimits(self, anotherbubble, t, zmid, rb1, rb2):

        dx = anotherbubble.x0 - self.x0
        dy = anotherbubble.y0 - self.y0
        dz = anotherbubble.z0 - self.z0

        # Portion of the z distance that is 'ours'
        zz1 = zmid*rb1
        # And portion that is in the other bubble
        zz2 = zmid*rb1 - dz

        # Distance in x-y plane
        Rxy = math.sqrt(dx*dx + dy*dy)

        # Distance to intersection of bubble walls
        # like the radius of the bubble, but in the x-y plane only
        # will never fail, as zz is at most rb
        q1 = math.sqrt(rb1*rb1 - zz1*zz1)
        q2 = math.sqrt(rb2*rb2 - zz2*zz2)

        # Bubbles are on top of each other, only issue is z-plane range
        if Rxy < tol:
            return -math.pi, math.pi



        # if q1 is exactly zero (but not Rxy) then the bubbles are not
        # on top of each other but they do touch the top/bottom of
        # this one
        if q1 < tol:
            return -math.pi, math.pi

        # Angle C, angle from edge of collided region to Rxy line
        # cannot be bigger than 90 degrees because of above
        try:
            C = math.acos((q1*q1 + Rxy*Rxy - q2*q2)/(2*q1*Rxy))
        except ValueError:
            # happens if q1 or Rxy are very small but not totally zero
            return -math.pi, math.pi

        # Angle E: minimum angle between positive x-axis and bubble
        E = math.atan2(dy, dx) - C

        # Minimum and maximum values of phi
        return E, E+2*C




    # Take a pair of bubbles, a time and a z-coordinate and turn them into
    # first the phi range required, and then fix that to its principal value
    def getlimits(self, anotherbubble, t, z):

        # Before bubbles were nucleated, so no space occupied
        if t < self.t0 or t < anotherbubble.t0:
            return []

        # Radius of each bubble
        rb1 = self.vw*(t - self.t0)
        rb2 = anotherbubble.vw*(t - anotherbubble.t0)

        # minimum and maximum of z coordinate occupied by bubble
        zlimits = self.get_zlimits(anotherbubble, t, rb1, rb2)

        if zlimits == None:
            return []

        zdiff1, zdiff2 = zlimits

        # Should not need to swap, but just in case
        if zdiff1 > zdiff2:
            zdiff1_temp = zdiff2
            zdiff2 = zdiff1
            zdiff1 = zdiff1_temp

        # And if we now find we are out of the range of interest,
        # we can return an empty list
        if z < zdiff1 or z > zdiff2:
            return []


        # want to find phi1, phi2
        phi1, phi2 = self.get_philimits(anotherbubble, t, z, rb1, rb2)

        final_set1 = []
        final_set2 = []

        if phi1 > phi2:
            phi1_temp = phi2
            phi2 = phi1
            phi1 = phi1_temp

        if phi1 < -math.pi and phi2 > math.pi:
            sys.stderr.write('bubble %d: Impossible geometry!\n', bubble.index)
            sys.exit(1)
        elif phi1 > math.pi and phi2 > math.pi:
            final_set1.append(-2*math.pi + phi1)
            final_set2.append(-2*math.pi + phi2)
        elif phi1 < -math.pi:
            final_set1.append(-math.pi)
            final_set2.append(phi2)

            final_set1.append(2*math.pi + phi1)
            final_set2.append(math.pi)
        elif phi2 > math.pi:
            final_set1.append(phi1)
            final_set2.append(math.pi)

            final_set1.append(-math.pi)
            final_set2.append(-2*math.pi + phi2)
        else:
            final_set1.append(phi1)
            final_set2.append(phi2)

        return list(map(list, list(zip(*[final_set1, final_set2]))))
        


# NB: (Should probably allow a degree of tolerance on these)...
# 
# Determines whether the ranges range1 and range2 intersect. These
# are each interpreted as two-item lists [min,max].
def intersect(range1, range2):
    if range2[0] >= range1[0] and range2[0] <= range1[1]:
        return True
    elif range2[1] >= range1[0] and range2[1] <= range1[1]:
        return True

    return False
    


# Linear merging of ranges in O(N) time. Given a list of limits, each
# a two-item list [min,max], sorts and merges them into the minimal
# set of limits.
#
# Algorithm source:
# http://www.geeksforgeeks.org/merging-intervals
# sorts in linear time...
def merge(limits):

    # Trivial case
    if len(limits) == 0:
        return []

    # Check the input is correctly formed by the time we get here
    for limit in limits:
        if limit[0] > limit[1]:
            sys.stderr.write('ill-formed limits!\n')
            sys.exit(1)
        
    # sort using the lower limit as a key
    sorted_limits = sorted(limits, key=lambda interval: interval[0])


    merge_stack = []
    merge_stack.append(sorted_limits[0])
    
    for limit in sorted_limits[1:]:
        if not intersect(merge_stack[-1], limit):
            merge_stack.append(limit)
        elif merge_stack[-1][1] <= limit[1]:
            merge_stack[-1][1] = limit[1]

    return merge_stack



# Takes a set of merged limits of where the bubble _has_ collided in
# the range (-pi,pi), and turns it into a range of limits where the
# bubble _has not_ collided. The envelope approximation integrals are
# over the _uncollided_ region.
#
# We assume that the above merge algorithm has already been run.
def uncollided(merged_limits):

    # Full range is uncollided
    if len(merged_limits) == 0:
        return [[-math.pi,math.pi]]
    

    regions = []

    # First range starts at -math.pi
    regions.append([-math.pi, merged_limits[0][0]])

    # Zip truncates to the shortest in the list, so the last element
    # is taken off.
    for current, next in zip(merged_limits, merged_limits[1:]):
        regions.append([current[1],next[0]])

    # Last range ends at math.pi
    regions.append([merged_limits[-1][1], math.pi])

    return regions




# Using the above 'merge' and 'uncollided' functions, returns minimal
# (merged) set of uncollided regions for a given z (between -1 and 1)
# and time, between a given bubble and a set of (other) bubbles.
def get_uncollided_regions(thisbubble, bubble_list, my_t, z):

    rb = thisbubble.vw*(my_t - thisbubble.t0)
    
    all_limits = []
        
    for bubble in bubble_list:
        all_limits = all_limits + thisbubble.getlimits(bubble, my_t, z)

    limits_merged = merge(all_limits)
        
    # Turn collided region limits into _uncollided_ region limits
    uncollided_regions = uncollided(limits_merged)

    return uncollided_regions



# Get values of Bplus and Bminus integrals, for a given time and
# z-coordinate.  This is the integral in A5 of Huber and
# Konstandin. We look at the overall uncollided region, so we need the
# list of all other bubbles.
def get_bplusminus(bubble, bubble_list, my_t, z):
    uncollided_regions = get_uncollided_regions(bubble, bubble_list,
                                                my_t, z)

    # No way of flagging up if range is exactly 2pi, but still check,
    # because then we know that the integral is exactly zero. This
    # check should have been done earlier but we double check here
    # just in case.
    if len(uncollided_regions) == 1 \
            and abs(uncollided_regions[0][0] + math.pi) < tol \
            and abs(uncollided_regions[0][1] - math.pi) < tol:
        return 0.0, 0.0

    bplus = 0.0
    bminus = 0.0

    for limits in uncollided_regions:
        lower = limits[0]
        upper = limits[1]
        
        bplus += 0.5*math.sin(2.0*upper) - 0.5*math.sin(2.0*lower)
        bminus += -0.5*math.cos(2.0*upper) + 0.5*math.cos(2.0*lower)

    return ((1.0-z*z)/2.0)*bplus, ((1.0-z*z)/2.0)*bminus


# For splitting into integration subregions adaptively.
#
# Looks at the previous iteration of a numerical valuation of the A-integral
# (eq. A.4 in Huber and Konstandin) and determines regions where the integral
# is within tolerances of zero. Should be used with care, and only on
# high sample densities.
def get_z_ranges(z_list, aplus_list, aminus_list):

    start = None

    ranges = []

    # If the first value is immediately nonzero, that is where
    # the first integral begins
    if abs(aplus_list[0]) > tol or abs(aminus_list[0]) > tol:
        start = z_list[0]

    # Then, run through the z-coordinates
    for i in range(1,len(z_list)-1):

        # If we're not inside a range that is nonzero but
        # this iterate is nonzero, so the previous entry is the
        # safe start of the integration region
        if (abs(aplus_list[i]) > tol or abs(aminus_list[i]) > tol) \
                and start == None:

            start = z_list[i-1]

        # If we're the first zero element after a range of nonzero,
        # then we have found the upper limit and we should at it to
        # the list of ranges and reset "start" to None.
        elif (abs(aplus_list[i]) < tol and abs(aminus_list[i]) < tol) \
                and not (start == None):

            ranges.append((start,z_list[i]))
            start = None

    # If we have an open range when we finish, then we need to add it,
    # up to the upper limit of the integration.
    if not (start == None):

        ranges.append((start,z_list[-1]))

    if len(ranges) == 1:
        sys.stderr.write('z range `%s` cannot be split!\n' % repr(ranges))
        ranges = [(ranges[0][0],(ranges[0][0] + ranges[0][1])/2.0),((ranges[0][0] + ranges[0][1])/2.0,ranges[0][1])]
        sys.stderr.write('z range divided into two: %s\n' % repr(ranges))
    elif len(ranges) == 0:
        sys.stderr.write('note: no z ranges found!\n')

    return ranges



zlist_master = {}
bplus_bare_list_master = {}
bminus_bare_list_master = {}

# Computes the Aplus/Aminus integrals for a given range
# (this could go on forever, but in practice rarely does)
# TODO: check for excessive recursion
def get_aplusminus_range(bubble, bubble_list, omega, t, zmin, zmax, kmin, kmax, level=1):


    aminus_last = None
    aplus_last = None
    diff = None

    aplus_list = []
    aminus_list = []

    zlist_last = []
    bplus_list_last = []
    bminus_list_last = []

    bplus_bare_list_last = []
    bminus_bare_list_last = []

    # ???? increasing this seems to pull down the late rise, more than
    # tolerance should
    #kmin = 4
    #kmax = 15

    dz = None

    for k in range(kmin, kmax+1):
        nsamples = pow(2,k) + 1

        if nsamples > 5000:
            sys.stderr.write('bubble %d: warning: omega=%g, t=%g, using %d samples\n' % (bubble.index, omega, t, nsamples))
            sys.stderr.write('bubble %d: splitting into parts: z (%g,%g) - bplus (%g,%g) - bminus (%g,%g)\n' % (bubble.index,
                                                                                                                zlist_last[0], zlist_last[-1], abs(bplus_list_last[0]),
                                                                                                                abs(bplus_list_last[-1]), abs(bminus_list_last[0]),
                                                                                                                abs(bminus_list_last[-1])))
            zranges = get_z_ranges(zlist_last,
                                   bplus_list_last, bminus_list_last)


            sys.stderr.write('bubble %d: got zranges %s\n' % (bubble.index,repr(zranges)))
            sys.stderr.write('bubble %d: (NB: get_aplusminus_range at level %d)\n' % (bubble.index, level))


            aplus_tot = 0.0
            aminus_tot = 0.0

            # Recursion on each subdivided range
            for zrange in zranges:
                sys.stderr.write('bubble %d: processing zrange %s\n' % (bubble.index,repr(zrange)))

                aplus, aminus = get_aplusminus_range(bubble, bubble_list,
                                                     omega, t,
                                                     zrange[0], zrange[1], kmin, kmax, level+1)
                aplus_tot += aplus
                aminus_tot += aminus

                sys.stderr.write('bubble %d: range (%g,%g): real %g imag %g\n'
                                 % (bubble.index, zrange[0], zrange[1],
                                    aplus.real, aplus.imag))

            sys.stderr.write('bubble %d: done: part splitting successful, with aplus_tot real %g, imag %g\n' % (bubble.index, aplus_tot.real, aplus_tot.imag))
            return aplus_tot, aminus_tot

        # if the number of samples does not require recursion, just
        # evaluate the integral

        zlist = None
        bplus_bare_list = None
        bminus_bare_list = None

        t_int = int(round(t,10)*1e10)
        zmin_int = int(round(zmin,10)*1e10)
        zmax_int = int(round(zmax,10)*1e10)
        try:

            zlist = zlist_master[(bubble,nsamples,t,zmin_int,zmax_int)]
            bplus_bare_list = bplus_bare_list_master[(bubble,nsamples,t,
                                                      zmin_int,zmax_int)]
            bminus_bare_list = bminus_bare_list_master[(bubble,nsamples,t,
                                                        zmin_int,zmax_int)]


            bplus_list = []
            bminus_list = []

            for z, bplus, bminus in zip(zlist,
                                        bplus_bare_list, bminus_bare_list):
                expon = cmath.exp(-1J*bubble.vw*omega*(t-bubble.t0)*z)
                bplus_list.append(expon*bplus)
                bminus_list.append(expon*bminus)

            dz = (zmax - zmin)/float(nsamples-1)

        except KeyError:

            zlist = []
            bplus_bare_list = []
            bminus_bare_list = [] 
            bplus_list = []
            bminus_list = []

            for sample in range(nsamples):

                z = None
                bplus = None
                bminus = None


                dz = (zmax - zmin)/float(nsamples-1)

        

                z = zmin + sample*dz

                zlist.append(z)

                # Reuse points from last iteration, as each time we add
                # 2^(n-1) points
                if sample % 2 == 0 and len(zlist_last) > 0:
                    bplus_list.append(bplus_list_last[sample//2])
                    bminus_list.append(bminus_list_last[sample//2])
                    bplus_bare_list.append(bplus_bare_list_last[sample//2])
                    bminus_bare_list.append(bminus_bare_list_last[sample//2])
                else:
                    # Otherwise just get B integral directly
                    bplus, bminus = get_bplusminus(bubble, bubble_list, t, z)

                    expon = cmath.exp(-1J*bubble.vw*omega*(t-bubble.t0)*z)

                    bplus_list.append(expon*bplus)
                    bminus_list.append(expon*bminus)
                    bplus_bare_list.append(bplus)
                    bminus_bare_list.append(bminus)


            zlist_master[(bubble,nsamples,t,zmin_int,zmax_int)] = zlist
            bplus_bare_list_master[(bubble,nsamples,t,zmin_int,zmax_int)] \
                = bplus_bare_list
            bminus_bare_list_master[(bubble,nsamples,t,zmin_int,zmax_int)] \
                = bminus_bare_list


        # Remember what we just computed in case we need to use it on
        # the next iteration.
        zlist_last = zlist
        bplus_list_last = bplus_list
        bminus_list_last = bminus_list
        bplus_bare_list_last = bplus_bare_list
        bminus_bare_list_last = bminus_bare_list


        # Romberg is good enough, and it works more reliably for us as
        # we don't have to provide a callback to the function of
        # interest.
        aminus = scipy.integrate.romb(bminus_list, dz)
        aplus = scipy.integrate.romb(bplus_list, dz)


        # We use the last computed integrand as our comparison for
        # relative tolerance convergence tests.
        if aminus_last == None:

            aminus_last = aminus
            aplus_last = aplus

        else:

            # If the integrals are _exactly_ zero, stop
            # -- this won't change by increasing samples
            if aplus == 0.0 and aminus == 0.0:
                break

            # This is an imperfect way of doing it, we should perhaps
            # check separately the convergence of aplus and aminus.
            diff_plus = abs(abs(aplus_last) - abs(aplus))/abs(aplus)
            diff_minus =  abs(abs(aminus_last) - abs(aminus))/abs(aminus)

            aplus_last = aplus
            aminus_last = aminus

            aplus_list.append(aplus)
            aminus_list.append(aminus)

            # This comparison is vitally important, determines the
            # quality of the resulting power spectrum.
            if (diff_plus < 1e-3 or abs(aplus) < 1e-10) and (diff_minus < 1e-3 or abs(aminus) < 1e-10):
                if nsamples  > 10000:
                    sys.stderr.write('bubble %d: and this is what it looked like:\n' % bubble.index)
                    sys.stderr.write('bubble %d aplus: ' + repr(aplus_list) + '\n' % bubble.index)
                    sys.stderr.write('bubble %d: aminus: ' + repr(aminus_list) + '\n' % bubble.index)
                break
            # Or if they are within tolerances and were last time
#            elif abs(aplus) < tol and abs(aminus) < tol \
#                    and abs(aplus_last) < tol and abs(aminus_last) < tol:
#                break


    # Bad news.... did not converge with our given number of samples.
    # Now with the recursion above I do not believe this is ever reached.
    else:
        sys.stderr.write('bubble %d: omega*t = %g: did not converge adequately '
                         'on range (%g,%g), diff=%g, '
                         'aplus_last %g, samples %d\n' 
                         % (bubble.index, omega*t, zmin, zmax, diff,
                            abs(aplus_last), nsamples))

        sys.stderr.write('bubble %d: aplus: ' + repr(aplus_list) + '\n' % bubble.index)
        sys.stderr.write('bubble %d: aminus: ' + repr(aminus_list) + '\n' % bubble.index)
        sys.stderr.write('bubble %d: zlimits: ' % bubble.index) 
        for otherbubble in bubble_list:
            rb1 = bubble.vw*(t - bubble.t0)
            rb2 = otherbubble.vw*(t - otherbubble.t0)
            sys.stderr.write(repr(bubble.get_zlimits(otherbubble, t, rb1, rb2)) 
                             + ', ')
        sys.stderr.write('\n')

        for z, bp in zip(zlist_last, bplus_list_last):
            print(z, bp.real, bp.imag)
        sys.stdout.flush()
        sys.exit(1)


    # We are done (we broke out)
    return aplus_last, aminus_last



# Compute the aplus and aminus integrals given a frequency omega and time t
# for the collision of a bubble with all the other bubbles in bubble_list.
def get_aplusminus(bubble, bubble_list, omega, t, omega_max, kmin, kmax):

    # First, work out the smallest and largest z that we need to
    # investigate.
    zmin = None
    zmax = None

    for otherbubble in bubble_list:

        # Calculate bubble radii
        rb1 = bubble.vw*(t - bubble.t0)
        rb2 = otherbubble.vw*(t - otherbubble.t0)

        # The integral is zero outside the limit
        zrange = bubble.get_zlimits(otherbubble, t, rb1, rb2)

        # The bubble does not intersect otherbubble, or has not grown yet
        if zrange == None:
            continue

        zmin_this = zrange[0]
        zmax_this = zrange[1]

        if zmin == None or zmin_this < zmin:
            zmin = zmin_this
        if zmax == None or zmax_this > zmax:
            zmax = zmax_this

    if zmin == None and zmax == None:
        return 0.0, 0.0


    # Make the range a little bit bigger just to be sure
    zmin += tol
    zmax -= tol




    # Not needed because zlimits checks if the bubble intersects
#    if t < otherbubble.t0:
#        return 0.0, 0.0

    aplus_total = 0.0
    aminus_total = 0.0


    # look for places where cos(bubble.vw*omega*(t-bubble.t0)*z) = 0
    # and thus bubble.vw*omega*(t-bubble.t0)*z = 2*n*pi
    # between zmin and zmax


    # This are the minimum and maximum nodes that lie within the range
    nmin = int(math.ceil(-1*(bubble.vw*omega_max*(t-bubble.t0))
                          /(2.0*math.pi)))
    nmax = int(math.floor(+1*(bubble.vw*omega_max*(t-bubble.t0))
                           /(2.0*math.pi)))
    
    nodes = list(range(nmin,nmax+1,1))

    # Break the integration interval into periods of the complex
    # exponential, to allow higher frequencies better sampling. A more
    # sensible and durable alternative would be to use an integration
    # algorithm that is designed to cope with highly oscillatory
    # integrals, but this will do here for now.

    breaks = []

    for node in nodes:

        # Nodal value
        nval = node*2.0*math.pi/(bubble.vw*omega_max*(t-bubble.t0))

        if nval > zmin and nval < zmax:
            breaks.append(nval)


    # Turn the breaks into integration ranges, and go ahead and
    # integrate

#    sys.stderr.write('bubble %d: BREAKS ARE %s\n' % (bubble.index,`breaks`))

    zmin_here = zmin

    for zmax_here in breaks:

#        sys.stderr.write('bubble %d: doing interval (%g,%g)\n' % (bubble.index, zmin_here, zmax_here))

        aplus, aminus = get_aplusminus_range(bubble, bubble_list,
                                             omega, t,
                                             zmin_here, zmax_here, kmin, kmax, 1)

        aplus_total += aplus
        aminus_total += aminus

        zmin_here = zmax_here

    zmax_here = zmax

    # Do the final integration range

#    sys.stderr.write('bubble %d: doing final interval (%g,%g)\n' % (bubble.index, zmin_here, zmax_here))
    aplus, aminus = get_aplusminus_range(bubble, bubble_list,
                                         omega, t,
                                         zmin_here, zmax_here, kmin, kmax, 1)

    aplus_total += aplus
    aminus_total += aminus


    return aplus_total, aminus_total


# Do C integral from tstart to tmax for a single bubble
def get_cplusminus_bubble(bubble, bubble_list, omega, tmax, tstart, 
                          omega_max, samples, kmin, kmax, cutoff=False):

    sys.stderr.write('bubble %d: using %d samples for bubble\n' % (bubble.index, samples))

    aplus_list = []
    aminus_list = []

    # By default, use 512+1 steps because that's what Huber and Konstandin
    # happened to use.
    steps = samples+1

    # Timestep
    dt = (tmax-tstart)/float(steps-1)


    for T in range(0,steps,1):

        t = tstart + T*dt


        C = 1.0
        if cutoff:
            tc = 0.9*tmax
            if t > tc:
                t0 = (tmax-tc)/4.0
                C = math.exp(-((t-tc)**2.0)/(t0))

        expon = C*cmath.exp(1J*omega*(t-bubble.z0))*math.pow(t-bubble.t0,3.0)    
        aplus, aminus = get_aplusminus(bubble, bubble_list, omega, t, 
                                       omega_max, kmin, kmax)

        aplus_list.append(expon*aplus)
        aminus_list.append(expon*aminus)

    # Romberg integration again. This one basically has to be this
    # because of the layers of crap underneath :)
    cplus = scipy.integrate.romb(aplus_list, dt)
    cminus = scipy.integrate.romb(aminus_list, dt)

    cplus_fewer = scipy.integrate.romb(aplus_list[::2], 2.0*dt)
    cminus_fewer = scipy.integrate.romb(aminus_list[::2], 2.0*dt)

    diff = abs(abs(cplus_fewer) - abs(cplus))/abs(cplus) \
        + abs(abs(cminus_fewer) - abs(cminus))/abs(cminus)

    sys.stderr.write('bubble %d: done with bubble, omega=%g, diff=%g\n' %
                     (bubble.index, omega, diff))
    
    # Pi normalisation stuff.
    return cplus/(6.0*math.pi), cminus/(6.0*math.pi)



# Compute cplus and cminus integrals for all bubbles in the simulation
# volume.
def get_cplusminus(bubble, bubble_list, omega, t, tstart, omega_max, samples, kmin, kmax, cutoff):
    cminus_total = 0.0
    cplus_total = 0.0

    for otherbubble in bubble_list:

        cplus, cminus = get_cplusminus_bubble(bubble, bubble_list,
                                              omega, t, tstart,
                                              omega_max, samples, kmin, kmax,
                                              cutoff)

        cminus_total += cminus
        cplus_total += cplus

        # This doesn't make sense - there is no longer an iteration over bubbles...
        break

    return cplus_total, cminus_total



# For debugging, or to produce pretty maps, print a parametric
# (cylindrical coordinates) display of the areas that are uncollided for
# the given bubble at the given time.
def print_uncollided_regions(bubble, bubble_list, my_t=30):

    intervals = 100

    for Z in range(-intervals/2, (intervals/2)+1, 20):

        z = 0.5*Z/float(intervals)

        uncollided_regions = get_uncollided_regions(bubble, bubble_list,
                                                    my_t, z)
        
        for space in uncollided_regions:
            print(space[0], z)
            print(space[1], z)
            print()
            print()




# Compute image bubbles that lie outside the principal simulation
# volume (and therefore should not contribute to the power) but that
# correspond to around-the-world collisions.
#
# At the moment, only bubbles that are direct face neighbours (not
# diagonal) are included, which saves a lot of processing and should
# be okay except for a couple of edge cases (pardon the pun!).
def get_images(bubble, L):
    x_orig = bubble.x0
    y_orig = bubble.y0
    z_orig = bubble.z0
    vw_orig = bubble.vw
    t0_orig = bubble.t0

    image_list = []

    image_list.append(Bubble(vw=vw_orig,
                             x0=x_orig+L, y0=y_orig+0, z0=z_orig+0,
                             t0=t0_orig))

    image_list.append(Bubble(vw=vw_orig,
                             x0=x_orig+0, y0=y_orig+L, z0=z_orig+0,
                             t0=t0_orig))

#    image_list.append(Bubble(vw=vw_orig,
#                             x0=x_orig+L, y0=y_orig+L, z0=z_orig+0,
#                             t0=t0_orig))

#    image_list.append(Bubble(vw=vw_orig,
#                             x0=x_orig+L, y0=y_orig-L, z0=z_orig+0,
#                             t0=t0_orig))

    image_list.append(Bubble(vw=vw_orig,
                             x0=x_orig+0, y0=y_orig-L, z0=z_orig+0,
                             t0=t0_orig))

#    image_list.append(Bubble(vw=vw_orig,
#                             x0=x_orig-L, y0=y_orig-L, z0=z_orig+0,
#                             t0=t0_orig))

    image_list.append(Bubble(vw=vw_orig,
                             x0=x_orig-L, y0=y_orig+0, z0=z_orig+0,
                             t0=t0_orig))

#    image_list.append(Bubble(vw=vw_orig,
#                             x0=x_orig-L, y0=y_orig+L, z0=z_orig+0,
#                             t0=t0_orig))

#    image_list.append(Bubble(vw=vw_orig,
#                             x0=x_orig+L, y0=y_orig+0, z0=z_orig+L,
#                             t0=t0_orig))

#    image_list.append(Bubble(vw=vw_orig,
#                             x0=x_orig+0, y0=y_orig+L, z0=z_orig+L,
#                             t0=t0_orig))

#    image_list.append(Bubble(vw=vw_orig,
#                             x0=x_orig+L, y0=y_orig+L, z0=z_orig+L,
#                             t0=t0_orig))

#    image_list.append(Bubble(vw=vw_orig,
#                             x0=x_orig+L, y0=y_orig-L, z0=z_orig+L,
#                             t0=t0_orig))

#    image_list.append(Bubble(vw=vw_orig,
#                             x0=x_orig+0, y0=y_orig-L, z0=z_orig+L,
#                             t0=t0_orig))

#    image_list.append(Bubble(vw=vw_orig,
#                             x0=x_orig-L, y0=y_orig-L, z0=z_orig+L,
#                             t0=t0_orig))

#    image_list.append(Bubble(vw=vw_orig,
#                             x0=x_orig-L, y0=y_orig+0, z0=z_orig+L,
#                             t0=t0_orig))

#    image_list.append(Bubble(vw=vw_orig,
#                             x0=x_orig-L, y0=y_orig+L, z0=z_orig+L,
#                             t0=t0_orig))

    image_list.append(Bubble(vw=vw_orig,
                             x0=x_orig+0, y0=y_orig+0, z0=z_orig+L,
                             t0=t0_orig))

#    image_list.append(Bubble(vw=vw_orig,
#                             x0=x_orig+L, y0=y_orig+0, z0=z_orig-L,
#                             t0=t0_orig))

#    image_list.append(Bubble(vw=vw_orig,
#                             x0=x_orig+0, y0=y_orig+L, z0=z_orig-L,
#                             t0=t0_orig))

#    image_list.append(Bubble(vw=vw_orig,
#                             x0=x_orig+L, y0=y_orig+L, z0=z_orig-L,
#                             t0=t0_orig))

#    image_list.append(Bubble(vw=vw_orig,
#                             x0=x_orig+L, y0=y_orig-L, z0=z_orig-L,
#                             t0=t0_orig))

#    image_list.append(Bubble(vw=vw_orig,
#                             x0=x_orig+0, y0=y_orig-L, z0=z_orig-L,
#                             t0=t0_orig))

#    image_list.append(Bubble(vw=vw_orig,
#                             x0=x_orig-L, y0=y_orig-L, z0=z_orig-L,
#                             t0=t0_orig))

#    image_list.append(Bubble(vw=vw_orig,
#                             x0=x_orig-L, y0=y_orig+0, z0=z_orig-L,
#                             t0=t0_orig))

#    image_list.append(Bubble(vw=vw_orig,
#                             x0=x_orig-L, y0=y_orig+L, z0=z_orig-L,
#                             t0=t0_orig))

    image_list.append(Bubble(vw=vw_orig,
                             x0=x_orig+0, y0=y_orig+0, z0=z_orig-L,
                             t0=t0_orig))


    return image_list



# Use all the above to compute the power for a given set of bubbles
# (inside the box), image bubbles (counted for collisions but do not
# themselves contribute to the power), omega (frequency) and tmax
# (maximum time to which we integrate)
def get_power(bubble_list, image_list, omega, tmax, L, omega_max, samples, kmin, kmax, cutoff):
        cplus_total = 0.0
        cminus_total = 0.0

        for i in range(len(bubble_list)):
            sys.stderr.write('processing bubble %d/%d for omega %g\n'
                             % (i + 1, len(bubble_list), omega))

            cplus, cminus = get_cplusminus(bubble_list[i],
                                           bubble_list[:i] 
                                           + bubble_list[i+1:]
                                           + image_list,
                                           omega, tmax, 0.0,
                                           omega_max,
                                           samples,
                                           kmin,
                                           kmax,
                                           cutoff)

            cplus_total += cplus
            cminus_total += cminus

        sys.stderr.write('cplus total is (%g,%g)\n'
                         % (cplus_total.real,cplus_total.imag))

        integrand = abs(cplus_total)*abs(cplus_total) \
            + abs(cminus_total)*abs(cminus_total)

        # kappa, G, (?rhovac) scaled out
        norm = 4.0*omega*omega*math.pow(bubble_list[0].vw,6.0)

        # non-Huber-Konstandin factors: an extra omega
        # and a 4*pi so that the spherical monte carlo integration
        # is a direct average.
        extranorm = omega*4.0*math.pi/float(L*L*L)

        return extranorm*norm*integrand




# Parse the config file and turn it into a dictionary
def parse_config(filename):
#    rest = string.join(sys.argv[2:])
    rest = ''

    config_dict = {}

    for line in open(filename,'r').readlines():
        bits = line.strip().split()
        if len(bits) == 0 or bits[0][0] == '#':
            continue

        key = bits[0]
        value = bits[1]

        if key in ['tmax', 'vw','L']:
            value = float(value)
            config_dict[key] = value
        elif key in ['samples','kmin','kmax']:
            value = int(value)
            config_dict[key] = value
        elif key in ['bubblelist']:
            config_dict[key] = value
        elif key in ['omega']:
            bits = ' '.join(bits[1:]).split(',')
            lat = []
            for item in bits:
                lat.append(float(item.strip()))
            config_dict[key] = lat
        elif key in ['images','cutoff']:
            if value.lower() in ['1','on','true','yes']:
                config_dict[key] = True
            else:
                config_dict[key] = False
        else:
            sys.stderr.write('Warning: unrecognised parameter %s\n' % key)

    return config_dict




def main():

#    L = 3
#    tmax = 3
#    vw = 1.0

    bubble_list = []
    image_list = []

    # Read the file bublist for a list of the bubbles to simulate
    # (todo: make this also contain parameters)
    try:
        inputfile = sys.argv[1]
        config = parse_config(inputfile)
    except IndexError:
        sys.stderr.write('Usage: %s <input file>\n' % sys.argv[0])
        sys.exit(1)
    except IOError:
        sys.stderr.write('Could not open file %s\n' % inputfile)
        sys.exit(1)

        

    try:
        L = config['L']
        tmax = config['tmax']
        vw = config['vw']
        samples = config['samples']
        kmin = config['kmin']
        kmax = config['kmax']
        images = config['images']
        cutoff = config['cutoff']
    except KeyError as e:
        sys.stderr.write('essential parameters not specified: %s missing\n'
                         % e.message)
        sys.exit(1)

    sys.stderr.write('L=%d, tmax=%g, vw=%g\n' % (L, tmax, vw))

    try:
        sys.stderr.write('Reading bubbles from %s\n' % config['bubblelist'])
        lines = open(config['bubblelist'],'r').readlines()
    except KeyError:
        sys.stderr.write('bubble list not specified\n')
        sys.exit(1)

    bubble_index = 1

    for line in lines:

        line = line.strip()
        if len(line) == 0 or line[0] == '#':
            continue

        # A line of the input file should read:
        # "(x0,y0,z0) t0"

        # The coordinates come first
        coords_end = line.find(')')
        coords = line[:coords_end+1]

        # Then the nucleation time
        t0 = float(line[coords_end+1:])

        # Process the coordinates
        bits = coords[1:-1].split(',')

        if len(bits) == 3:
            x0 = float(bits[0])
            y0 = float(bits[1])
            z0 = float(bits[2])

            # sys.stderr.write('adding bubble at (%g,%g,%g), t0=%g\n'
            #                 % (x0,y0,z0,t0))

            # Instantiate a bubble and add it to the list
            bubble_list.append(Bubble(vw=vw, x0=x0, y0=y0, z0=z0, t0=t0, index=bubble_index))

            # And add the corresponding image bubbles
            if images:
                image_list += get_images(bubble_list[-1], L)

            bubble_index += 1




    # We assume that this run is a sample for a Monte Carlo integration
    # over the z-direction chosen for the cylindrical coordinate system.

    angle1 = random.uniform(-math.pi,math.pi)
    z = random.uniform(-1,1)
    angle2 = math.acos(z)
    angle3 = random.uniform(-math.pi,math.pi)

    sys.stderr.write('angles are: %g, %g, %g\n' % (angle1, angle2, angle3))

#    angle1 = 0.0
#    angle2 = math.pi/6.0
#    angle3 = 0.0

    for i in range(len(bubble_list)):
        bubble_list[i].rotateZYZ(angle1, angle2, angle3)

    for i in range(len(image_list)):
        image_list[i].rotateZYZ(angle1, angle2, angle3)


    # Print out a z-phi map of regions that will be integrated over at
    # a given time
    # print_uncollided_regions(bubble_list[0], image_list, 1.6)
    # return
    
    # Finally, make the power spectrum

    omega_max = max(config['omega'])

    for omega in config['omega']:

        sys.stderr.write('starting omega=%g\n' % omega)

        start = time.time()
        integrand = get_power(bubble_list, image_list, omega, tmax, L,
                              omega_max, samples, kmin, kmax, cutoff)
        end = time.time()

        print(omega, integrand)

        sys.stderr.write('omega=%g took %d seconds walltime\n'
                         % (omega, end-start))

        # We are impatient and it might take a while.
        sys.stdout.flush()


if __name__ == '__main__':
    main()
