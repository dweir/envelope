#!/usr/bin/env python

# PySpark version of envelope approximation calculation - uses envelope.py
# for nearly everything which does not need to be parallelised

from envelope import *

import math, sys, os, string, random, time
from pyspark import SparkContext
from operator import add





# Use all the above to compute the power for a given set of bubbles
# (inside the box), image bubbles (counted for collisions but do not
# themselves contribute to the power), omega (frequency) and tmax
# (maximum time to which we integrate)
def get_power_spark(bubble_list, image_list, omega, tmax, L, omega_max, samples, sc, nprocs):




#    allocs = fairshare(size, len(bubble_list))
#    myallocs = allocs[rank]
    
#    sys.stderr.write('Rank %d gets bubbles: %s\n' % (rank, `myallocs`))
        

    bubbleRDD = sc.parallelize(range(len(bubble_list)), nprocs)

    rlist = sc.runJob(bubbleRDD, lambda bubblePart: [get_cplusminus(bubble_list[i],
                                                                    bubble_list[:i]
                                                                    + bubble_list[i+1:]
                                                                    + image_list,
                                                                    omega,
                                                                    tmax, 0.0,
                                                                    omega_max,
                                                                    samples) for i in bubblePart])


### flattens too much :(
#    rlist = bubbleRDD.map(lambda i: get_cplusminus(bubble_list[i],
#                                                 bubble_list[:i]
#                                                 + bubble_list[i+1:]
#                                                 + image_list,
#                                                 omega,
#                                                 tmax, 0.0,
#                                                 omega_max,
#                                                 samples)).reduce(add)
    

    cplus_total = 0.0
    cminus_total = 0.0
        
    for cplus, cminus in rlist:
        cplus_total += cplus
        cminus_total += cminus

    sys.stderr.write('cplus total is (%g,%g)\n'
                     % (cplus_total.real,cplus_total.imag))

    integrand = abs(cplus_total)*abs(cplus_total) \
        + abs(cminus_total)*abs(cminus_total)

    # kappa, G, (?rhovac) scaled out
    norm = 4.0*omega*omega*math.pow(bubble_list[0].vw,6.0)

    # non-Huber-Konstandin factors: an extra omega
    # and a 4*pi so that the spherical monte carlo integration
    # is a direct average.
    extranorm = omega*4.0*math.pi/float(L*L*L)

    return extranorm*norm*integrand





def main():

    sys.stderr.write('2016 Envelope code: Spark version\n')

    bubble_list = []
    image_list = []

    # Read the file bublist for a list of the bubbles to simulate
    # (todo: make this also contain parameters)
    try:
        inputfile = sys.argv[1]
        nprocs = int(sys.argv[2])
        config = parse_config(inputfile)
    except (IndexError, ValueError) as e:
        sys.stderr.write('Usage: %s <input file> <nprocs for spark>\n' % sys.argv[0])
        sys.exit(1)
    except IOError as e:
        sys.stderr.write('Could not open file %s\n' % inputfile)
        sys.exit(1)

        

    try:
        L = config['L']
        tmax = config['tmax']
        vw = config['vw']
        samples = config['samples']
    except KeyError, e:
        sys.stderr.write('essential parameters not specified: %s missing\n'
                % e.message)
        sys.exit(1)

    sys.stderr.write('L=%d, tmax=%g, vw=%g\n' % (L, tmax, vw))

    try:
        sys.stderr.write('Reading bubbles from %s\n' % config['bubblelist'])
        lines = open(config['bubblelist'],'r').readlines()
    except KeyError:
        sys.stderr.write('bubble list not specified\n')
        sys.exit(1)

    bubble_index = 1

    for line in lines:

        line = string.strip(line)
        if len(line) == 0 or line[0] == '#':
            continue

        # A line of the input file should read:
        # "(x0,y0,z0) t0"

        # The coordinates come first
        coords_end = string.find(line,')')
        coords = line[:coords_end+1]

        # Then the nucleation time
        t0 = float(line[coords_end+1:])

        # Process the coordinates
        bits = string.split(coords[1:-1],',')

        if len(bits) == 3:
            x0 = float(bits[0])
            y0 = float(bits[1])
            z0 = float(bits[2])

            # sys.stderr.write('adding bubble at (%g,%g,%g), t0=%g\n'
            #                 % (x0,y0,z0,t0))

            # Instantiate a bubble and add it to the list
            bubble_list.append(Bubble(vw=vw, x0=x0, y0=y0, z0=z0, t0=t0, index=bubble_index))

            # And add the corresponding image bubbles
            image_list += get_images(bubble_list[-1], L)

            bubble_index += 1

    # We assume that this run is a sample for a Monte Carlo integration
    # over the z-direction chosen for the cylindrical coordinate system.

    angle1 = random.uniform(-math.pi,math.pi)
    z = random.uniform(-1,1)
    angle2 = math.acos(z)
    angle3 = random.uniform(-math.pi,math.pi)

    sys.stderr.write('angles are: %g, %g, %g\n' % (angle1, angle2, angle3))

    for i in range(len(bubble_list)):
        bubble_list[i].rotateZYZ(angle1, angle2, angle3)

    for i in range(len(image_list)):
        image_list[i].rotateZYZ(angle1, angle2, angle3)


    omega_max = max(config['omega'])

    # Finally, make the power spectrum

    sc = SparkContext(appName="Envelope")

    for omega in config['omega']:

        sys.stderr.write('starting omega=%g\n' % omega)

        start = time.time()
        integrand = get_power_spark(bubble_list, image_list, omega,
                                    tmax, L, omega_max, samples, sc, nprocs)
        end = time.time()

        sys.stdout.write('%g %g\n' % (omega, integrand))

        sys.stderr.write('omega=%g took %d seconds walltime\n' % (omega, end-start))

        # We are impatient and it might take a while.
        sys.stdout.flush()


if __name__ == '__main__':
    main()
