#!/usr/bin/env python

# Given standard envelope config file, generate list of points
# suitable for processing by qhull.
#
# The file dovoronoi.sh in the paper directory processes the output
# through qhull, with the following set of commands:
#
# for i in {0..109};
# do
#    cat qhull-input | qvoronoi QV$i p | qhull FS | tail -n1;
# done | awk '{print $2}' | awk '{s+=$1} END {print s}'
#
# where 109 is the number of bubbles in the 'simulation' volume
# and the output is the total surface area.
#
# NB: This file loads the bubbles as if they were going to be used
# in the envelope approximation simulations, then just spits out
# their positions again. Why do this? Because in principle
# we can also rotate them to test whether the Voronoi triangulation is
# accuate and independent of rotations (as it really should be!).

import math, sys
import string
import scipy.integrate
import cmath
import random
import time

from envelope import *



def main():
    bubble_list = []
    image_list = []

    # Read the file bublist for a list of the bubbles to simulate
    # (todo: make this also contain parameters)
    try:
        inputfile = sys.argv[1]
        config = parse_config(inputfile)
    except IndexError:
        sys.stderr.write('Usage: %s <input file>\n' % sys.argv[0])
        sys.exit(1)
    except IOError:
        sys.stderr.write('Could not open file %s\n' % inputfile)
        sys.exit(1)

        

    try:
        L = config['L']
        tmax = config['tmax']
        vw = config['vw']
    except KeyError, e:
        sys.stderr.write('essential parameters not specified: %s missing\n' 
                         % e.message)
        sys.exit(1)

    sys.stderr.write('L=%d, tmax=%g, vw=%g\n' % (L, tmax, vw))

    try:
        sys.stderr.write('Reading bubbles from %s\n' % config['bubblelist'])
        lines = open(config['bubblelist'],'r').readlines()
    except KeyError:
        sys.stderr.write('bubble list not specified\n')
        sys.exit(1)

    for line in lines:

        line = string.strip(line)
        if len(line) == 0 or line[0] == '#':
            continue

        # A line of the input file should read:
        # "(x0,y0,z0) t0"

        # The coordinates come first
        coords_end = string.find(line,')')
        coords = line[:coords_end+1]

        # Then the nucleation time
        t0 = float(line[coords_end+1:])

        # Process the coordinates
        bits = string.split(coords[1:-1],',')

        if len(bits) == 3:
            x0 = float(bits[0])
            y0 = float(bits[1])
            z0 = float(bits[2])

            # sys.stderr.write('adding bubble at (%g,%g,%g), t0=%g\n'
            #                 % (x0,y0,z0,t0))

            # Instantiate a bubble and add it to the list
            bubble_list.append(Bubble(vw=vw, x0=x0, y0=y0, z0=z0, t0=t0))

            # And add the corresponding image bubbles
            image_list += get_images(bubble_list[-1], L)

            ### below: various manipulations of the bubble directions ###

            # angle1 = random.uniform(-math.pi,math.pi)
            # z = random.uniform(-1,1)
            # angle2 = math.acos(z)
            # angle3 = random.uniform(-math.pi,math.pi)

            # sys.stderr.write('angles are: %g, %g, %g\n'
            #                 % (angle1, angle2, angle3))

            # angle1 = 0.0
            # angle2 = math.pi/6.0
            # angle3 = 0.0

            # for i in range(len(bubble_list)):
            #     bubble_list[i].rotateZYZ(angle1, angle2, angle3)

            # for i in range(len(image_list)):
            #     image_list[i].rotateZYZ(angle1, angle2, angle3)


    # Qvoronoi format follows:

    # 1. the dimension
    print '3'

    # 2. the number of points
    print `len(bubble_list) + len(image_list)`

    # 3. point coordinates
    for item in bubble_list:
        print '%g %g %g' % (item.x0, item.y0, item.z0)
    for item in image_list:
        print '%g %g %g' % (item.x0, item.y0, item.z0)


if __name__ == '__main__':
    main()
