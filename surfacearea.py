#!/usr/bin/env python

# compute uncollided surface area at a given time
#
# this can be used to calculate an effective 'u-bar-phi' for the envelope
# approximation
#
# note that the accuracy is principally set by the variable 'intervals',
# which is 100 here but might be increased to (say) 10000


import math, sys
import string
import scipy.integrate
import cmath
import random
import time
from envelope import *


# Use get_uncollided_regions from envelope.py to get the total
# uncollided surface area, for a _single_ bubble, at a time my_t
def sum_uncollided_regions(bubble, bubble_list, my_t=30):

    if my_t < bubble.t0:
        return 0.0

    intervals = 100

    total = 0.0

    for Z in range(-intervals//2, (intervals//2), 1):

        z = 2.0*Z/float(intervals)
        theta = math.acos(z)
        thetaplus = math.acos(z+2.0/float(intervals))
        dangle = math.sin(theta)*abs(thetaplus-theta)


        uncollided_regions = get_uncollided_regions(bubble, bubble_list,
                                                    my_t, z)
        
        for space in uncollided_regions:
            total += abs(space[1]-space[0])*dangle

    return total*math.pow(bubble.vw*(my_t - bubble.t0),2.0)


def main():

    bubble_list = []
    image_list = []


    # Read the file bublist for a list of the bubbles to simulate
    # (todo: make this also contain parameters)
    try:
        inputfile = sys.argv[1]
        config = parse_config(inputfile)
        evaltime = float(sys.argv[2])
    except IndexError:
        sys.stderr.write('Usage: %s <input file> <evaluation time>\n'
                         % sys.argv[0])
        sys.exit(1)
    except IOError:
        sys.stderr.write('Could not open file %s\n' % inputfile)
        sys.exit(1)

        

    try:
        L = config['L']
        tmax = config['tmax']
        vw = config['vw']
        images = config['images']
    except KeyError as e:
        sys.stderr.write('essential parameters not specified: %s missing\n' 
                         % e.message)
        sys.exit(1)

    sys.stderr.write('L=%d, tmax=%g, vw=%g\n' % (L, tmax, vw))

    try:
        sys.stderr.write('Reading bubbles from %s\n' % config['bubblelist'])
        lines = open(config['bubblelist'],'r').readlines()
    except KeyError:
        sys.stderr.write('bubble list not specified\n')
        sys.exit(1)

    for line in lines:

        line = line.strip()
        if len(line) == 0 or line[0] == '#':
            continue

        # A line of the input file should read:
        # "(x0,y0,z0) t0"

        # The coordinates come first
        coords_end = line.find(')')
        coords = line[:coords_end+1]

        # Then the nucleation time
        t0 = float(line[coords_end+1:])

        # Process the coordinates
        bits = coords[1:-1].split(',')

        if len(bits) == 3:
            x0 = float(bits[0])
            y0 = float(bits[1])
            z0 = float(bits[2])

            # sys.stderr.write('adding bubble at (%g,%g,%g), t0=%g\n'
            #                 % (x0,y0,z0,t0))

            # Instantiate a bubble and add it to the list
            bubble_list.append(Bubble(vw=vw, x0=x0, y0=y0, z0=z0, t0=t0))

            # And add the corresponding image bubbles
            if images:
                image_list += get_images(bubble_list[-1], L)



    # We assume that this run is a sample for a Monte Carlo integration
    # over the z-direction chosen for the cylindrical coordinate system.

    # Note that the surface area seems to have a weak dependence
    # on the angle, which it shouldn't have!

    angle1 = random.uniform(-math.pi,math.pi)
    z = random.uniform(-1,1)
    angle2 = math.acos(z)
    angle3 = random.uniform(-math.pi,math.pi)

    sys.stderr.write('angles are: %g, %g, %g\n' % (angle1, angle2, angle3))

    # One particular choice of angle that might be of use
    #    angle1 = 0.0
    #    angle2 = math.pi/6.0
    #    angle3 = 0.0

    # Rotate all the bubbles
    for i in range(len(bubble_list)):
        bubble_list[i].rotateZYZ(angle1, angle2, angle3)

    for i in range(len(image_list)):
        image_list[i].rotateZYZ(angle1, angle2, angle3)


    # Add up all the uncollided regions for all the bubbles at the time.
    total = 0.0
    for i in range(len(bubble_list)):
        total += sum_uncollided_regions(bubble_list[i], 
                                        bubble_list[:i]
                                        + bubble_list[i+1:]
                                        + image_list,
                                        evaltime)
    print(total)


if __name__ == '__main__':
    main()
