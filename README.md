David Weir's envelope code
==========================

This is a code for computing the gravitational wave signal in the
envelope approximation. The computations are based on _Gravitational
Wave Production by Collisions: More Bubbles_ by Stephan J. Huber and
Thomas Konstandin
[[arXiv:0806.1828](https://arxiv.org/abs/0806.1828)], with additional
details from the earlier papers _Gravitational radiation from
colliding vacuum bubbles: envelope approximation to many bubble
collisions_ by Arthur Kosowsky and Michael S. Turner
[[arXiv:astro-ph/9211004](https://arxiv.org/abs/astro-ph/9211004)] and
_Gravitational radiation from colliding vacuum bubbles_ [[Inspire link
with preprint](https://inspirehep.net/literature/324187)]. In the latter
paper the envelope approximation is introduced in Appendix C.

The results of this code have, where appropriate, been validated
against the results of the above papers.

This code, or an earlier version of it, was used in the production of
the following papers:

- _Vacuum bubble collisions: from microphysics to gravitational waves_
  by Oliver Gould, Satumaaria Sukuvaara, and David J. Weir (in
  preparation).
- _Revisiting the envelope approximation: gravitational waves from
  bubble collisions_ by David J. Weir
  [[arXiv:1604.08429](https://arxiv.org/abs/1604.08429)].
  
Snapshots of the source code used for these two papers, and of the
data files (and error output) will be uploaded to Zenodo. Originally
this repo contained all the output and error files for
arXiv:1604.08429 but to make the repo manageable for public release,
the error files were stripped from the history using the following command:

```
git filter-branch --force --prune-empty  --index-filter "git rm -f
--cached --ignore-unmatch run/*err*" --tag-name-filter cat -- --all
```

The output files from these runs can, however, be found in the
repository's history. All output and error files will be uploaded to
Zenodo.


Which version to use; prerequisites
-----------------------------------

- All versions require [SciPy](https://www.scipy.org/) for
  `scipy.integrate`. Interestingly when I wrote this code I didn't
  use NumPy much, because of the need to build lists of uncollided
  regions.
- **The MPI version is the preferred one**, and requires
  [mpi4py](https://pypi.org/project/mpi4py/).
- The PySpark version (in the `deprecated/` directory) is a proof of
  concept, and not currently up-to-date (may not run with Python
  3). You can read more about my experiences with PySpark
  [here](https://saoghal.net/articles/2016/pyspark-for-hpc-some-success-but-a-work-in-progress/).

Running
-------

The `run/example` directory has a sample configuration file `config`
(containing comments which document the necessary details). This
configuration file, or the others, can be passed as an argument to
`envelope.py`, `envelope_mpi.py` or `envelope_spark.py`.


Further reading
---------------

- PySpark for HPC: some success but a work in progress:
  https://saoghal.net/articles/2016/pyspark-for-hpc-some-success-but-a-work-in-progress/
- Gravitational Wave Production by Collisions: More Bubbles:
  https://arxiv.org/abs/0806.1828
- Gravitational radiation from colliding vacuum bubbles: envelope
approximation to many bubble collisions:
https://arxiv.org/abs/astro-ph/9211004
- Gravitational radiation from colliding vacuum bubbles:
  https://inspirehep.net/literature/324187
- Revisiting the envelope approximation: gravitational waves from
  bubble collisions: https://arxiv.org/abs/1604.08429
  
